import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given(/^que eu esteja logado na página de cursos$/, () => {
  return true;
});

When(/^clico na opção Adicionar outro curso$/, () => {
  return true;
});

When(/^clicar no botão Iniciar do novo curso$/, () => {
  return true;
});

When(/^clicar no botão Iniciar da aula$/, () => {
  return true;
});

Then(/^serei direcionado para as aulas do curso$/, () => {
  return true;
});
