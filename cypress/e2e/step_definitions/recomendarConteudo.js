import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given(/^que eu esteja na lista de alunos de uma turma existente$/, () => {
  return true;
});

When(/^clico nas opções Recomendações e Recomendar$/, () => {
  return true;
});

When(/^clico no botão Recomendar$/, () => {
  return true;
});

When(/^faço as configurações das recomendações$/, () => {
  return true;
});

Then(/^será mostrado uma mensagem de que realizou as recomendações de x itens para y alunos$/, () => {
  return true;
});
