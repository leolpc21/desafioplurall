import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given(/^que eu esteja na tela inicial$/, () => {
  cy.visit('/')
});

When(/^eu acessar a opção de Entrar$/, () => {
  cy.get('#login-or-signup')
    .click()
  cy.url()
    .should('be.equal', Cypress.config().baseUrl + '/login')
});

When(/^eu preencher as informações de usuário$/, () => {
  cy.get('input[data-test-id="identifier-field"]')
    .type(Cypress.env('login'), { log: false })
  cy.get('input[data-test-id="password-field"]')
    .type(Cypress.env('password'), { log: false })
  cy.get('button[data-test-id="log-in-submit-button"]')
    .click()
});

Then(/^serei direcionado para a página do dashboard$/, () => {
  cy.url()
    .should('be.equal', Cypress.config().baseUrl + '/teacher/dashboard')
});