/// <reference types="cypress" />
import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
import utils from "../../support/utils";
const fakerBr = require("faker-br");

const elements = {
  buttonNext: 'button[data-test-id="go-next-button"]',
  buttonNewClass: 'a[data-test-id="add-new-class"]',
  buttonCreateStudents: 'div[data-test-id="create-students-without-email"]',
  buttonAddNewLine: 'button._z9asim',
  buttonCreatorSkip: 'button[data-test-id="class-creator-skip-button"]',
  courseClass: '._stzw5vv ._1w0kk064',
  inputClassName: 'input[data-test-id="class-name-input-field"]',
  inputNewStudents: 'input[data-test-id="new-student-name-',
  tableStudents: 'table._1n78j0nb tr ._phvnl3',
  tableListStudents: 'a[data-test-id="student-name"]',
  titleModal: 'h1[data-test-id="modal-title"]',
  titleModalAlterPassword: 'h3._1n0v3gfc',
}

Given(/^que eu esteja logado na pagina do dashboard$/, () => {
  cy.loginUser()
});

When(/^acessar a opção de Adicionar nova turma$/, () => {
  cy.get(elements.buttonNewClass)
    .should('be.visible')
    .click()
  cy.get(elements.titleModal)
    .contains('Adicionar nova turma')
    .should('be.visible')
});

When(/^preencher os dados da turma$/, () => {
  Cypress.env('nomeTurma', 'Turma Teste ' + utils.functionDataHoraAtual())
  cy.intercept('POST', 'https://pt.khanacademy.org/api/internal/graphql/getClassList**').as('classList')
  cy.intercept('POST', 'https://pt.khanacademy.org/api/internal/graphql/createClassroomMutation**').as('createClassroomMutation')

  cy.get(elements.inputClassName)
    .type(Cypress.env('nomeTurma'))
  cy.get(elements.buttonNext)
    .click()
    .wait('@createClassroomMutation')
    .its('response')
    .then(function (response) {
      Cypress.env('codeClass', response.body['data']['createClassroom']['classroom']['signupCode'])
    })
  cy.get(elements.courseClass)
    .contains('Matemática EF: 1º Ano')
    .click()
  cy.get(elements.buttonNext)
    .should('be.visible')
    .click()
    .esperar('classList', 200)
});

When(/^criar três contas de alunos$/, () => {
  var idNewStudents = 0
  cy.intercept('POST', 'https://pt.khanacademy.org/api/internal/graphql/getSuggestedUsername**').as('suggestedUsername')

  cy.get(elements.buttonCreateStudents).click()

  Cypress._.times(3, () => {
    Cypress.env(`nomeAluno${idNewStudents}`, `${fakerBr.name.firstName()} ${fakerBr.name.lastName()}`)

    cy.get(elements.inputNewStudents + `${idNewStudents}"]`)
      .type(Cypress.env(`nomeAluno${idNewStudents}`))
      .esperar('suggestedUsername', 200)

    cy.get(elements.buttonAddNewLine).click()

    idNewStudents += 1
  })

  cy.get(elements.buttonNext)
    .should('be.visible')
    .click()
  cy.get(elements.titleModalAlterPassword)
    .contains('Opcional: edite as senhas dos seus alunos')
    .should('be.visible')

  var idNewStudents = 0

  Cypress._.times(3, () => {
    cy.get(elements.tableStudents)
      .contains(Cypress.env(`nomeAluno${idNewStudents}`))
      .should('be.visible')

    idNewStudents += 1
  })

  cy.get(elements.buttonNext)
    .should('be.visible')
    .click()
  cy.get(elements.buttonCreatorSkip)
    .should('be.visible')
    .click()
});

When(/^clicar no botão Veja sua turma$/, () => {
  cy.get(elements.buttonNext)
    .should('be.visible')
    .click()
});

Then(/^será aberta a pagina da turma na listagem de aluno$/, () => {
  var idNewStudents = 0

  cy.url()
    .should('be.equal', Cypress.config().baseUrl + '/teacher/class/' + Cypress.env('codeClass') + '/students')
  cy.get('h4').contains(Cypress.env('nomeTurma'))

  Cypress._.times(3, () => {
    cy.get(elements.tableListStudents)
      .contains(Cypress.env(`nomeAluno${idNewStudents}`))
      .should('be.visible')

    idNewStudents += 1
  })
});
