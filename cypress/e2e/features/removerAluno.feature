Feature: Remover aluno de uma turma existente

  Como um professor logado
  Eu quero remover aluno de uma turma existente
  Para que o aluno não tenha acesso as conteúdos da turma

  Scenario: Remover aluno de uma turma existente
    Given que eu esteja na listage de alunos de uma turma existente
    And seleciono um aluno
    When clicar em Remover da turma
    Then o aluno será removido da turma