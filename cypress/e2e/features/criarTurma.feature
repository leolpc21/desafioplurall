Feature: Criar Turma com Alunos

  Como um professor logado
  Eu quero criar uma nova turma
  Para inserir meus novos alunos

  Scenario: Criar uma nova turma com alunos
    Given que eu esteja logado na pagina do dashboard
    And acessar a opção de Adicionar nova turma
    And preencher os dados da turma
    And criar três contas de alunos
    When clicar no botão Veja sua turma
    Then será aberta a pagina da turma na listagem de aluno