Feature: Recomendar conteúdo para uma turma existente

  Como um professor logado
  Eu quero recomendar conteúdos para uma turma existente
  Para que os alunos tenham acesso

  Scenario: Recomendar conteúdos para uma turma existente
    Given que eu esteja na lista de alunos de uma turma existente
    And clico nas opções Recomendações e Recomendar
    And clico no botão Recomendar
    When faço as configurações das recomendações
    Then será mostrado uma mensagem de que realizou as recomendações de x itens para y alunos