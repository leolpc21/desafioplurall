
Feature: Login

    Como um professor cadastrado
    Eu quero realizar o login
    Para acessar o dashboard

    Scenario: Realizar o login
        Given que eu esteja na tela inicial
        And eu acessar a opção de Entrar
        When eu preencher as informações de usuário
        Then serei direcionado para a página do dashboard
