Feature: Entrar em uma turma e acessar suas aulas

  Como um aluno logado
  Eu quero entrar em uma nova turma
  Para acessar suas aulas

  Scenario: Acessar as aulas de uma turma
    Given que eu esteja logado na página de cursos
    And clico na opção Adicionar outro curso
    And clicar no botão Iniciar do novo curso
    When clicar no botão Iniciar da aula
    Then serei direcionado para as aulas do curso