/// <reference types="cypress" />

Cypress.Commands.add('loginUser', () => {

  cy.intercept('POST', 'https://pt.khanacademy.org/api/internal/graphql/getTeacherCampaignProgressForBanner**').as('userProfile')

  cy.visit('/')
  cy.get('#login-or-signup').click()
  cy.url()
    .should('be.equal', Cypress.config().baseUrl + '/login')
  cy.get('input[data-test-id="identifier-field"]')
    .type(Cypress.env('login'), { log: false })
  cy.get('input[data-test-id="password-field"]')
    .type(Cypress.env('password'), { log: false })
  cy.get('button[data-test-id="log-in-submit-button"]')
    .click()
    .esperar('userProfile', 200)
  cy.url()
    .should('be.equal', Cypress.config().baseUrl + '/teacher/dashboard')

})

Cypress.Commands.add("esperar", function (wait, status) {
  cy.wait(`@${wait}`, { timeout: 20000 })
    .its("response.statusCode")
    .should("eq", status);
});

Cypress.on("uncaught:exception", () => {
  return false;
});
