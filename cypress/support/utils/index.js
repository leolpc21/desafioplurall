

class GenericsUtils {
  functionDataHoraAtual() {

    const data = new Date();

    const dataAtual = `${data.getDate()}${data.getMonth() + 1}${data.getFullYear()}`;

    const horaAtual = `${data.getHours()}${data.getMinutes()}${data.getSeconds()}`

    return dataAtual + ' ' + horaAtual;
  }

}

export default new GenericsUtils();