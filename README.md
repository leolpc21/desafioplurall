# desafioPlurall

### Instalação:

`npm install`


### Comandos personalizados para rodar os testes automatizados

Para rodar os testes via tela rodar o comando `npm run test`. Será aberto a tela do Cypress, escolhe a opção 'E2E Testing', escolhe o navegador e 'Start E2E Testing in ...'.

Para rodar os testes em modo headless rodar o comando `npm run cy:run` no terminal.


### Arquivos de feature

Arquivos de feature na pasta `cypress/e2e/features`


### Arquivos de cenários automatizados de testes

Arquivos de cenários automatizados na pasta `cypress/e2e/steps_definitions`


### Comandos personalizados Cypress

Arquivos de comandos personalizados na pasta `cypress/support`


### Funções genéricas personalizadas

Arquivos de funções personalizadas na pasta `cypress/support/utils`


### Reporte de inconsistência

<a href="https://gitlab.com/leolpc21/desafioplurall/uploads/1697a4625d254a6d9e9040f51a5bad31/Report_de_inconsistência.pdf">Reporte de inconsistência</a>